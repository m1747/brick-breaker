import {detect_collision} from "/src/collision_detection";

export default class Brick {
  constructor(game, position){
    this.image = document.getElementById("brick");

    this.game = game;

    this.position = position;

    this.width = 80;
    this.height = 24;

    this.marked_for_remove = false;
  }

  update(delta_time){
    if (detect_collision(this.game.ball, this)){
      this.game.ball.speed.y = -this.game.ball.speed.y
      this.marked_for_remove = true;
    }
  }

  draw(ctx){
    ctx.drawImage(this.image, this.position.x, this.position.y, this.width, this.height);
  }

}