export function detect_collision(ball, ob){

  let top_ball = ball.position.y 
  let bottom_ball = top_ball + ball.size;

  let left_ball = ball.position.x;
  let right_ball = left_ball + ball.size;

  let top_ob = ob.position.y;
  let bottom_ob = top_ob + ob.height;

  let left_ob = ob.position.x;
  let right_ob = left_ob + ob.width;

  if (bottom_ball >= top_ob && top_ball <= bottom_ob &&
      left_ball >= left_ob && right_ball <= right_ob){
      return true;
  } else {
    return false;
  }
}